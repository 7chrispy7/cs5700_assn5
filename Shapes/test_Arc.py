from unittest import TestCase
from PIL import ImageDraw, Image

import sys
sys.path.append('../')
from Arc import Arc


class TestArc(TestCase):
    def test_scale(self):
        a = Arc('Shape,Arc,0,0,1,0,90')
        a.scale(2)
        assert (a.origin == (0, 0) and a.radius == 2)

    def test_translate(self):
        a = Arc('Shape,Arc,0,0,1,0,90')
        a.translate(1, 1)
        assert (a.origin == (1, 1))

    def test_save(self):
        a = Arc('Shape,Arc,0,0,1,0,90')
        a.save('TestObjects/arc.txt')
        f = open('Objects/TestObjects/arc.txt')
        test_string = f.read()
        assert (test_string == 'Shape,Arc,0,0,1,0,90')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        arc = Arc('Shape,Arc,0,0,1,0,90')
        assert (arc.add_to_canvas(canvas) == canvas)

    def test_rotate(self):
        arc = Arc('Shape,Arc,0,0,1,0,90')
        arc.rotate(45)
        assert(arc.angles == (45, 135))
