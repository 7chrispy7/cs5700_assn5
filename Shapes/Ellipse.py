from PIL import Image, ImageDraw
from Shape import Shape
import math


class Ellipse(Shape):
    origin = (0, 0)
    size = (0, 0)

    def __init__(self, string):
        string = string.split(',')
        self.origin = (int(string[2]), int(string[3]))
        self.size = (int(string[4]), int(string[5]))

    def scale(self, factor):
        new_width = self.size[0] * factor
        new_height = self.size[1] * factor
        self.size = (new_width, new_height)

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return (self.size[0] * self.size[1])/4.0 * math.pi

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGBA', (max(100, self.origin[0] + 1), max(100, self.origin[1] + 1)), (255, 255, 255, 1))
        draw = ImageDraw.Draw(img)
        self.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        canvas.ellipse([self.origin[0] - self.size[0]/2.0, self.origin[1] - self.size[1]/2.0,
                        self.origin[0] + self.size[0]/2.0, self.origin[1] + self.size[1]/2.0], outline='black')
        return canvas

    def get_string(self):
        return 'Shape,Ellipse,' + str(self.origin[0]) + ',' + str(self.origin[1]) + ',' + \
               str(self.size[0]) + ',' + str(self.size[1])

    def rotate(self, degrees):
        pass