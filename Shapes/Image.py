from Shape import Shape
from PIL import Image as ImagePIL
from PIL import ImageDraw


class Image(Shape):
    origin = (0, 0)
    size = (0, 0)
    image_name = ''
    flyweight = None
    rotation = 0

    def __init__(self, string, flyweight):
        string = string.split(',')
        self.image_name = string[6]
        self.origin = (int(string[2]), int(string[3]))
        self.size = (int(string[4]), int(string[5]))
        self.flyweight = flyweight

    def scale(self, factor):
        self.size = (self.size[0] * factor, self.size[1] * factor)
        return self

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return self.size[0] * self.size[1]

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = ImagePIL.new('RGBA',
                           (max(100, self.origin[0] + self.size[0]), max(100, self.origin[1] + self.size[1])),
                           (255, 255, 255, 1))
        canvas = ImageDraw.Draw(img)
        self.add_to_canvas(canvas)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        return self.flyweight.add_to_canvas(self.origin, self.size, self.image_name, self.rotation, canvas)

    def get_string(self):
        return 'Shape,Image,' + str(self.origin[0]) + ',' + str(self.origin[1]) + \
                ',' + str(self.size[0]) + ',' + str(self.size[1]) + ',' + self.image_name

    def rotate(self, degrees):
        self.rotation = degrees
