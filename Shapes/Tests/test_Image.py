from unittest import TestCase
import os

import sys
sys.path.append('../')
from Image import Image
from ImageFlyweight import ImageFlyweight



class TestImage(TestCase):
    def test_scale(self):
        flyweight = ImageFlyweight()
        flyweight.add_image('test.png')
        image = Image('Shape,Image,10,10,50,50,test.png', flyweight)
        image.scale(0.5)
        assert(image.size == (25, 25) and image.origin == (10,10))

    def test_translate(self):
        flyweight = ImageFlyweight()
        flyweight.add_image('test.png')
        image = Image('Shape,Image,10,10,50,50,test.png', flyweight)
        image.translate(25, 25)
        assert(image.size == (50, 50) and image.origin == (35, 35))

    def test_get_area(self):
        flyweight = ImageFlyweight()
        flyweight.add_image('test.png')
        image = Image('Shape,Image,10,10,50,50,test.png', flyweight)
        assert(image.get_area() == 2500)

    def test_save(self):
        flyweight = ImageFlyweight()
        flyweight.add_image('test.png')
        image = Image('Shape,Image,10,10,50,50,test.png', flyweight)
        os.chdir('..')
        image.save('TestObjects/image.txt')
        f = open('Objects/TestObjects/image.txt')
        test_string = f.read()
        assert(test_string == 'Shape,Image,10,10,50,50,test.png')
