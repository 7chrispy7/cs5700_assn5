from unittest import TestCase
from PIL import Image, ImageDraw

import sys
sys.path.append('../')
from Shapes.Text import Text


class TestText(TestCase):
    def test_translate(self):
        text = Text('Shape,Text,0,0,This is a text')
        text.translate(10, 10)
        assert(text.origin == (10, 10))
        assert(text.text == 'This is a text')

    def test_save(self):
        t = Text('Shape,Text,0,0,This is text')
        t.save('TestObjects/text.txt')
        f = open('Objects/TestObjects/text.txt')
        test_string = f.read()
        assert (test_string == 'Shape,Text,0,0,This is text')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        text = Text('Shape,Text,0,0,This is text')
        assert (text.add_to_canvas(canvas) == canvas)
