from unittest import TestCase
from PIL import Image, ImageDraw
import math

import sys
sys.path.append('../')
from Circle import Circle


class TestCircle(TestCase):
    def test_scale(self):
        c = Circle('Shape,Circle,0,0,1')
        c.scale(2)
        assert(c.origin == (0, 0) and c.radius == 2)

    def test_translate(self):
        c = Circle('Shape,Circle,0,0,1')
        c.translate(1, 1)
        assert (c.origin == (1, 1))

    def test_get_area(self):
        c = Circle('Shape,Circle,0,0,3')
        assert(c.get_area() == 9 * math.pi)

    def test_save(self):
        c = Circle('Shape,Circle,0,0,1')
        c.save('TestObjects/circle.txt')
        f = open('Objects/TestObjects/circle.txt')
        test_string = f.read()
        assert (test_string == 'Shape,Circle,0,0,1')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        circle = Circle('Shape,Circle,0,0,1')
        assert (circle.add_to_canvas(canvas) == canvas)
