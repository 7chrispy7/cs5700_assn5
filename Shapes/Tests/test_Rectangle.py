from unittest import TestCase
from PIL import Image, ImageDraw
import os

import sys
sys.path.append('../')
from Rectangle import Rectangle



class TestRectangle(TestCase):
    def test_scale(self):
        rect = Rectangle('Shape,Rectangle,0,0,1,1')
        rect.scale(2)
        test = Rectangle('Shape,Rectangle,0,0,2,2')
        assert (rect.origin == test.origin and rect.point1 == test.point1)

    def test_translate(self):
        rect = Rectangle('Shape,Rectangle,0,0,1,1')
        rect.translate(1, 1)
        test = Rectangle('Shape,Rectangle,1,1,2,2')
        assert (rect.origin == test.origin and rect.point1 == test.point1)

    def test_get_area(self):
        rect = Rectangle('Shape,Rectangle,0,0,3,2')
        assert(rect.get_area() == 6)

    def test_save(self):
        rect = Rectangle('Shape,Rectangle,0,0,1,1')
        rect.save('TestObjects/rect.txt')
        f = open('Objects/TestObjects/rect.txt')
        test_string = f.read()
        assert (test_string == 'Shape,Rectangle,0,0,1,1')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        rect = Rectangle('Shape,Rectangle,0,0,1,1')
        assert (rect.add_to_canvas(canvas) == canvas)

    def test_check_frame(self):
        rect = Rectangle('Shape,Rectangle,-10,-10,-9,-9')
        rect.check_frame()
        test = Rectangle('Shape,Rectangle,0,0,1,1')
        assert (rect.origin == test.origin and rect.point1 == test.point1)
