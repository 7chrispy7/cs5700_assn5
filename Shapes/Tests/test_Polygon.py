from unittest import TestCase
from PIL import ImageDraw, Image

import sys
sys.path.append('../')
from Polygon import Polygon


class TestPolygon(TestCase):
    def test_scale(self):
        polygon = Polygon('Shape,Polygon,0,0,10,0,0,10,10,10')
        polygon.scale(2)
        assert(polygon.origin == (0, 0))
        assert(polygon.points[0] == (20, 0))
        assert(polygon.points[1] == (0, 20))
        assert(polygon.points[2] == (20, 20))

    def test_translate(self):
        polygon = Polygon('Shape,Polygon,0,0,10,0,0,10,10,10')
        polygon.translate(5, 5)
        assert(polygon.origin == (5, 5))
        assert (polygon.points[0] == (15, 5))
        assert (polygon.points[1] == (5, 15))
        assert (polygon.points[2] == (15, 15))

    def test_get_area(self):
        polygon = Polygon('Shape,Polygon,0,0,10,0,0,10,10,10')
        assert(polygon.get_area() == 0)

    def test_save(self):
        polygon = Polygon('Shape,Polygon,0,0,0,1,1,1')
        polygon.save('TestObjects/polygon.txt')
        f = open('Objects/TestObjects/polygon.txt')
        test_string = f.read()
        assert(test_string == 'Shape,Polygon,0,0,0,1,1,1')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        polygon = Polygon('Shape,Polygon,0,0,0,1,1,1')
        assert (polygon.add_to_canvas(canvas) == canvas)

    def test_check_frame(self):
        polygon = Polygon('Shape,Polygon,-10,-10,-10,-5,-5,-5')
        polygon.check_frame()
        test = Polygon('Shape,Polygon,0,0,0,5,5,5')
        assert (polygon.origin == test.origin and polygon.points[0] == test.points[0] and
                polygon.points[1] == test.points[1])
