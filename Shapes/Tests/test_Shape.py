from unittest import TestCase
from PIL import Image, ImageDraw
import os

import sys
sys.path.append('../')
from Shape import Shape



class TestShape(TestCase):
    def test_scale(self):
        s = Shape()
        test = s.scale(1)
        assert(s == test)

    def test_translate(self):
        s = Shape()
        s.translate(1, 1)
        assert(s.origin == (1, 1))

    def test_get_area(self):
        s = Shape()
        assert(s.get_area() == 0)

    def test_save(self):
        s = Shape()
        s.save('TestObjects/shape.txt')
        f = open('Objects/TestObjects/shape.txt')
        test_string = f.read()
        assert(test_string == 'Shape,None,0,0')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        s = Shape()
        assert(s.add_to_canvas(canvas) == canvas)
