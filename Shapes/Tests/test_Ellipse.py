from unittest import TestCase
from PIL import Image, ImageDraw
import math
import os

import sys
sys.path.append('../')
from Ellipse import Ellipse


class TestEllipse(TestCase):
    def test_scale(self):
        e = Ellipse('Shape,Ellipse,0,0,2,1')
        e.scale(2)
        assert(e.origin == (0, 0) and e.size == (4, 2))

    def test_translate(self):
        e = Ellipse('Shape,Ellipse,0,0,1,1')
        e.translate(1, 1)
        assert (e.origin == (1, 1))

    def test_get_area(self):
        e = Ellipse('Shape,Ellipse,0,0,3,2')
        assert(e.get_area() == 1.5 * math.pi)

    def test_save(self):
        e = Ellipse('Shape,Ellipse,0,0,1,2')
        e.save('TestObjects/ellipse.txt')
        f = open('Objects/TestObjects/ellipse.txt')
        test_string = f.read()
        assert (test_string == 'Shape,Ellipse,0,0,1,2')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        ellipse = Ellipse('Shape,Ellipse,0,0,1,1')
        assert (ellipse.add_to_canvas(canvas) == canvas)
