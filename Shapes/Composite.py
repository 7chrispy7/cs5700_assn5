from Shape import Shape
from PIL import Image, ImageDraw

class Composite(Shape):
    origin = (0, 0)
    shapes = []

    def __init__(self):
        self.shapes = []

    def scale(self, factor):
        for shape in self.shapes:
            shape.scale(factor)
            translate_x = -shape.origin[0] + (shape.origin[0] * factor)
            translate_y = -shape.origin[1] + (shape.origin[1] * factor)
            shape.translate(translate_x, translate_y)
        return self

    def translate(self, x, y):
        for shape in self.shapes:
            shape.translate(x, y)
        return self

    def get_area(self):
        area = 0
        for shape in self.shapes:
            area += shape.get_area()
        return area

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGBA', (100, 100), (255, 255, 255, 1))
        draw = ImageDraw.Draw(img)
        for shape in self.shapes:
            shape.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        for shape in self.shapes:
            shape.add_to_canvas(canvas)
        return canvas

    def get_string(self):
        shape_string = 'Shape,Composite'
        for shape in self.shapes:
            shape_string += '(' + shape.get_string() + ')'
        return shape_string

    def add_shape(self, shape):
        self.shapes.append(shape)

    def remove_shape(self, index):
        self.shapes.pop(index)

    def clear(self):
        self.shapes.clear()

    def rotate(self, degrees):
        for shape in self.shapes:
            shape.rotate(degrees)
            shape.origin = self.rotate_point(degrees, shape.origin)
