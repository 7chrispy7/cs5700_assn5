from Shape import Shape
from PIL import ImageDraw, Image


class Polygon(Shape):
    origin = (0, 0)
    points = []

    def __init__(self, string):
        self.points = []
        points_list = string.split(',')[2:]
        if len(points_list) < 6 or len(points_list) % 2 != 0:
            print('Error!! Invalid number of points.')
        else:
            self.origin = (int(points_list.pop(0)), int(points_list.pop(0)))
            while points_list:
                self.points.append((int(points_list.pop(0)), int(points_list.pop(0))))
            self.check_frame()

    def scale(self, factor):
        for point in range(len(self.points)):
            delta_x = self.points[point][0] - self.origin[0]
            delta_y = self.points[point][1] - self.origin[1]
            delta_x *= factor
            delta_y *= factor
            self.points[point] = (self.origin[0] + delta_x, self.origin[1] + delta_y)
        self.check_frame()
        return self

    def translate(self, x, y):
        new_x1 = self.origin[0] + x
        new_y1 = self.origin[1] + y
        self.origin = (new_x1, new_y1)
        for point in range(len(self.points)):
            new_x = self.points[point][0] + x
            new_y = self.points[point][1] + y
            self.points[point] = (new_x, new_y)
        self.check_frame()
        return self

    def get_area(self):
        return 0

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        max_x = 0
        max_y = 0
        for point in self.points:
            if point[0] > max_x:
                max_x = point[0]
            if point[1] > max_y:
                max_y = point[1]
        img = Image.new('RGBA',
                        (max(100, self.origin[0] + 1, max_x), max(100, self.origin[1] + 1, max_y)),
                        (255, 255, 255, 1))
        draw = ImageDraw.Draw(img)
        self.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        points_list = self.points
        points_list.insert(0, self.origin)
        canvas.polygon(points_list, outline='black')
        return canvas

    def check_frame(self):
        if self.origin[0] < 0:
            self.translate(-self.origin[0], 0)
        if self.origin[1] < 0:
            self.translate(0, -self.origin[1])
        for point in self.points:
            if point[0] < 0:
                self.translate(-point[0], 0)
            if point[1] < 0:
                self.translate(0, -point[1])

    def get_string(self):
        string = 'Shape,Polygon,{},{}'.format(self.origin[0], self.origin[1])
        for point in self.points:
            string += ',' + str(point[0]) + ',' + str(point[1])
        return string

    def rotate(self, degrees):
        for point in range(len(self.points)):
            self.points[point] = self.rotate_point(degrees, point)
        self.check_frame()
