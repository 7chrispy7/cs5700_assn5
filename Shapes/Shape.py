from PIL import Image, ImageDraw
import math


class Shape(object):
    origin = (0, 0)
    rotation = 0

    def __init__(self):
        self.rotation = 0

    def scale(self, factor):
        return self

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return 0

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGBA', (100, 100), (255, 255, 255, 1))
        # draw = ImageDraw.Draw(img)  # This line is not needed for the parent shape.
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        return canvas

    def get_string(self):
        return 'Shape,None,' + str(self.origin[0]) + ',' + str(self.origin[1])

    def rotate_point(self, degrees, (x, y)):
        x -= self.origin[0]
        y -= self.origin[1]
        degrees = math.radians(degrees)
        x_prime = x*math.cos(degrees) - y*math.sin(degrees)
        y_prime = y*math.cos(degrees) + x*math.sin(degrees)
        x_prime += self.origin[0]
        y_prime += self.origin[1]
        return (x_prime, y_prime)

    def rotate(self, degrees):
        pass
