from PIL import Image, ImageDraw
from Shape import Shape
import math


class Arc(Shape):
    origin = (0, 0)
    radius = 0
    angles = (0, 0)

    def __init__(self, string):
        string = string.split(',')
        self.origin = (int(string[2]), int(string[3]))
        self.radius = int(string[4])
        self.angles = (int(string[5]), int(string[6]))

    def scale(self, factor):
        self.radius *= factor

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return 0

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGB', (max(100, self.origin[0] + self.radius), max(100, self.origin[1] + self.radius)), (255, 255, 255))
        draw = ImageDraw.Draw(img)
        self.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        canvas.arc([self.origin[0] - self.radius, self.origin[1] - self.radius,
                    self.origin[0] + self.radius, self.origin[1] + self.radius], 
                   self.angles[0], self.angles[1], fill='black')
        return canvas

    def get_string(self):
        return 'Shape,Arc,' + str(self.origin[0]) + ',' + str(self.origin[1]) + ',' + \
               str(self.radius) + ',' + str(self.angles[0]) + ',' + str(self.angles[1])

    def rotate(self, degrees):
        angle1 = self.angles[0] + degrees
        angle2 = self.angles[1] + degrees
        if angle1 > 360:
            angle1 -= 360
        elif angle1 < 0:
            angle1 += 360
        if angle2 > 360:
            angle2 -= 360
        elif angle2 < 0:
            angle2 += 360
        self.angles = (angle1, angle2)
