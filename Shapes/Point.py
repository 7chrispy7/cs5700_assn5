from Shape import Shape
from PIL import Image, ImageDraw


class Point(Shape):
    origin = (0, 0)

    def __init__(self, string):
        string = string.split(',')
        self.origin = (max(int(string[2]), 0), max(int(string[3]), 0))

    def scale(self, factor):
        return self

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return 0

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGBA', (max(100, self.origin[0]+1), max(100, self.origin[1]+1)), (255, 255, 255, 1))
        draw = ImageDraw.Draw(img)
        self.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        canvas.point([self.origin[0], self.origin[1]], fill='black')
        return canvas

    def get_string(self):
        return 'Shape,Point,' + str(self.origin[0]) + ',' + str(self.origin[1])

    def rotate(self,degrees):
        pass
