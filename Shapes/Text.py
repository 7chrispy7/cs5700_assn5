from PIL import Image, ImageDraw
from Shape import Shape
import math


class Text(Shape):
    origin = (0, 0)
    text = ''

    def __init__(self, string):
        string = string.split(',')
        self.origin = (int(string[2]), int(string[3]))
        self.text = string[4]

    def scale(self, factor):
        pass

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return 0

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGB', (max(100, self.origin[0] + self.radius), max(100, self.origin[1] + self.radius)), (255, 255, 255))
        draw = ImageDraw.Draw(img)
        self.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        canvas.text([self.origin[0], self.origin[1]], self.text, fill='black')
        return canvas

    def get_string(self):
        return 'Shape,Text,' + str(self.origin[0]) + ',' + str(self.origin[1]) + ',' + self.text

    def rotate(self, degrees):
        pass
